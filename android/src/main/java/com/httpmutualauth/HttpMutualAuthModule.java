package com.httpmutualauth;

import android.support.annotation.RequiresPermission;
import android.support.annotation.VisibleForTesting;
import android.util.ArrayMap;
import android.util.Base64;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableType;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.util.HashMap;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class HttpMutualAuthModule extends ReactContextBaseJavaModule {


    private final ReactApplicationContext reactContext;
    public OkHttpClient baseClient;

    public HttpMutualAuthModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        this.baseClient = new OkHttpClient();
    }

    @Override
    public String getName() {
        return "HttpMutualAuth";
    }

    @ReactMethod
    public void get(ReadableMap options, final Callback callback) {

        try {

            String url = options.getString("url");
            HashMap<String, Object> setup = socketTrustSetup(options);
            X509TrustManager trustManager = (X509TrustManager)setup.get("trustManager");
            SSLSocketFactory socketFactory = (SSLSocketFactory)setup.get("socketFactory");

            OkHttpClient client = baseClient
                    .newBuilder()
                    .sslSocketFactory(socketFactory, trustManager)
                    .build();

            Request request = new Request.Builder().url(url).get().build();

            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(Call call, IOException ex) {
                    callback.invoke("Connection error: " + ex.toString(), null);
                }
                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    callback.invoke(null, response.body().string());
                }
            });

        } catch(Exception ex) {
            callback.invoke("Parameter error: " + ex.toString(), null);
        }
    }

    public HashMap<String, Object> socketTrustSetup(ReadableMap options) throws Exception {

        HashMap<String, Object> result = new HashMap<>();
        char[] keystorePassword = "".toCharArray();
        String keystoreBase64Enc = options.getString("keystore");
        byte[] keystoreBase64Dec = Base64.decode(keystoreBase64Enc, Base64.DEFAULT);
        String trustManagerAlgor = TrustManagerFactory.getDefaultAlgorithm();

        if(options.hasKey("password") && options.getType("password") == ReadableType.String) {
            keystorePassword = options.getString("password").toCharArray();
        }

        KeyStore store = KeyStore.getInstance("PKCS12");
        store.load(new ByteArrayInputStream(keystoreBase64Dec), keystorePassword);

        KeyManagerFactory kmf = KeyManagerFactory.getInstance("X509");
        kmf.init(store, keystorePassword);

        TrustManagerFactory tmf = TrustManagerFactory.getInstance(trustManagerAlgor);
        tmf.init((KeyStore) null); // use system default keystore

        KeyManager[] keyManagers = kmf.getKeyManagers();
        TrustManager[] trustManagers = tmf.getTrustManagers();

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(keyManagers, trustManagers, null);

        result.put("trustManager", trustManagers[0]);
        result.put("socketFactory", sslContext.getSocketFactory());

        return result;
    }
}
