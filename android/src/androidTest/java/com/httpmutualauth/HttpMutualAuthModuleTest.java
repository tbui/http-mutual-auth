package com.httpmutualauth;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;
import android.util.Base64;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.facebook.soloader.SoLoader;

import java.util.HashMap;

import org.apache.commons.io.IOUtils;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.mock.MockInterceptor;
import okhttp3.mock.Rule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(AndroidJUnit4.class)
@MediumTest
public class HttpMutualAuthModuleTest {

    private HttpMutualAuthModule module;
    private Context instrumentationCtx;

    @Before
    public void setUp() {

        module = new HttpMutualAuthModule(null);
        instrumentationCtx = InstrumentationRegistry.getContext();

        SoLoader.init(instrumentationCtx, false);

    }

    @Test
    public void shouldCreateTrustManagerAndFactoryWithValidPassword() {

        try {

            byte[] keystore = IOUtils.toByteArray(getClass().getResourceAsStream("/fake-bundle-with-pass.p12"));
            WritableMap options = Arguments.createMap();
            options.putString("keystore", Base64.encodeToString(keystore, Base64.DEFAULT));
            options.putString("password", "password");

            HashMap<String, Object> setup = module.socketTrustSetup(options);
            X509TrustManager trustManager = (X509TrustManager)setup.get("trustManager");
            SSLSocketFactory socketFactory = (SSLSocketFactory)setup.get("socketFactory");

            assertNotNull(trustManager);
            assertNotNull(socketFactory);

        } catch(Exception e) {

            assertNull(e);

        }

    }

    @Test
    public void shouldNotCreateTrustManagerAndFactoryWithInvalidPassword() {

        try {

            byte[] keystore = IOUtils.toByteArray(getClass().getResourceAsStream("/fake-bundle-with-pass.p12"));
            WritableMap options = Arguments.createMap();
            options.putString("keystore", Base64.encodeToString(keystore, Base64.DEFAULT));
            options.putString("password", "password1");
            HashMap<String, Object> setup = null;
            String message = "";

            try {

                setup = module.socketTrustSetup(options);

            } catch(Exception e) {

                message = e.getMessage();

            }

            assertEquals(message, "PKCS12 key store mac invalid - wrong password or corrupted file.");

            assertNull(setup);

        } catch(Exception e) {

            assertNull(e);

        }

    }

    @Test
    public void shouldNotRequirePasswordToCreateTrustManagerAndFactory() {

        try {

            HashMap<String, Object> setup;
            X509TrustManager trustManager;
            SSLSocketFactory socketFactory;

            byte[] keystore = IOUtils.toByteArray(getClass().getResourceAsStream("/fake-bundle-no-pass.p12"));
            WritableMap options = Arguments.createMap();
            options.putString("keystore", Base64.encodeToString(keystore, Base64.DEFAULT));

            setup = module.socketTrustSetup(options);
            trustManager = (X509TrustManager)setup.get("trustManager");
            socketFactory = (SSLSocketFactory)setup.get("socketFactory");

            assertNotNull(trustManager);
            assertNotNull(socketFactory);

            options.putString("password", "");

            setup = module.socketTrustSetup(options);
            trustManager = (X509TrustManager)setup.get("trustManager");
            socketFactory = (SSLSocketFactory)setup.get("socketFactory");

            assertNotNull(trustManager);
            assertNotNull(socketFactory);

            options.putNull("password");

            setup = module.socketTrustSetup(options);
            trustManager = (X509TrustManager)setup.get("trustManager");
            socketFactory = (SSLSocketFactory)setup.get("socketFactory");

            assertNotNull(trustManager);
            assertNotNull(socketFactory);

        } catch(Exception e) {

            assertNull(e);

        }

    }

    @Test
    public void shouldRespondToRequestsWithSuccessfulMutualAuthentication() {

        try {

            byte[] keystore = IOUtils.toByteArray(getClass().getResourceAsStream("/fake-bundle-with-pass.p12"));
            WritableMap options = Arguments.createMap();
            options.putString("keystore", Base64.encodeToString(keystore, Base64.DEFAULT));
            options.putString("password", "password");
            options.putString("url", "https://www.foobar.com/".toString());

            module.baseClient = new HttpMutualAuthModuleFakeHttpClient();

            MockInterceptor interceptor = new MockInterceptor();

            interceptor.addRule(new Rule.Builder()
                .get()
                .url("https://www.foobar.com/")
                .respond("it worked!"));

            ((HttpMutualAuthModuleFakeHttpClient)module.baseClient).addInterceptor(interceptor);

            module.get(options, new HttpMutualAuthModuleCallback() {
                @Override
                public void run() {
                Object err = data[0];
                Object res = data[1];
                assertEquals(null, err);
                assertEquals("it worked!", res);
                }
            });

        } catch(Exception e) {

            assertNull(e);

        }

    }

}
