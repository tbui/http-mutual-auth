package com.httpmutualauth;

import java.util.Iterator;
import java.util.LinkedList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;

public class HttpMutualAuthModuleFakeHttpClient extends OkHttpClient {

    LinkedList<Interceptor> mocks;

    HttpMutualAuthModuleFakeHttpClient() {
        super();
        mocks = new LinkedList<>();
    }

    @Override
    public Builder newBuilder() {
        Builder builder = super.newBuilder();
        Iterator<Interceptor> iterator = mocks.iterator();
        while(iterator.hasNext()) {
            builder.addInterceptor(iterator.next());
        }
        return builder;
    }

    public void addInterceptor(Interceptor interceptor) {
        mocks.add(interceptor);
    }

}
