package com.httpmutualauth;

import com.facebook.react.bridge.Callback;

abstract class HttpMutualAuthModuleCallback implements Callback, Runnable {

    protected Object[] data;

    @Override
    public void invoke(Object... args) {

        data = args;
        run();

    }

}
