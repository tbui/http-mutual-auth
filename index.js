import { NativeModules } from 'react-native';

const { HttpMutualAuth } = NativeModules;

export default {

  get: opts => {

    return new Promise((resolve, reject) => {

      HttpMutualAuth.get(opts, (err, data) => {

        if(err) {
          reject(err);
        } else {
          resolve(data);
        }

      });

    });

  }

};
