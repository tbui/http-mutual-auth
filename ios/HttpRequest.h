#import <React/RCTBridgeModule.h>
#import <Foundation/Foundation.h>
#import <React/RCTConvert.h>

@interface HttpRequest : NSObject <NSURLSessionTaskDelegate> {

    NSURL *_url;
    NSString *_password;
    NSData *_keystore;
    NSMutableData *_response;
    NSException *_exception;
    RCTResponseSenderBlock _callback;

}

- (id)initWithParams:(NSDictionary *)opts callback:(RCTResponseSenderBlock)callback;
- (void)get;
- (NSURLCredential *)socketTrustSetup;

@end
