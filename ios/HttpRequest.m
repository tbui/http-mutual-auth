#import "HttpRequest.h"

@implementation HttpRequest

- (id)initWithParams:(NSDictionary *)opts callback:(RCTResponseSenderBlock)callback {

    self = [super init];
    
    @try {
        if([opts objectForKey:@"password"] != nil) {
            _password = [RCTConvert NSString:opts[@"password"]];
        } else {
            _password = @"";
        }
        _callback = callback;
        _keystore = [[NSData alloc] initWithBase64EncodedString:[RCTConvert NSString:opts[@"keystore"]] options:0];
        _url = [NSURL URLWithString:[RCTConvert NSString:opts[@"url"]]];
    } @catch (NSException *exception) {
        callback(@[[NSString stringWithFormat:@"%@%@", @"Parameter error: ", exception.reason], NSNull.null]);
    }

    return self;

}

- (void)get {

    NSURLSession *session = [NSURLSession sessionWithConfiguration:NSURLSessionConfiguration.defaultSessionConfiguration delegate:self delegateQueue:NSOperationQueue.mainQueue];
    NSURLRequest *request = [NSURLRequest requestWithURL:_url];

    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {

        if (_exception) {

            _callback(@[[NSString stringWithFormat:@"%@%@", @"Connection error: ", _exception.reason], NSNull.null]);

        } else if (error) {

            _callback(@[[NSString stringWithFormat:@"%@%@", @"Connection error: ", error.localizedDescription], NSNull.null]);

        } else {

            _callback(@[NSNull.null, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]]);

        }

    }];
    
    [task resume];

}

- (NSURLCredential *)socketTrustSetup {
    
    CFStringRef password = (__bridge CFStringRef)_password;
    const void *keys[] = { kSecImportExportPassphrase };
    const void *values[] = { password };
    CFDictionaryRef optionsDictionary = CFDictionaryCreate(NULL, keys, values, 1, NULL, NULL);
    CFArrayRef p12Items;
    OSStatus result = SecPKCS12Import((__bridge CFDataRef)_keystore, optionsDictionary, &p12Items);

    if (result == noErr) {

        CFDictionaryRef identityDict = CFArrayGetValueAtIndex(p12Items, 0);
        SecIdentityRef identityApp = (SecIdentityRef)CFDictionaryGetValue(identityDict, kSecImportItemIdentity);
        
        SecCertificateRef certRef;
        SecIdentityCopyCertificate(identityApp, &certRef);
        
        SecCertificateRef certArray[1] = { certRef };
        CFArrayRef myCerts = CFArrayCreate(NULL, (void*)certArray, 1, NULL);
        CFRelease(certRef);
        
        NSURLCredential *credential = [NSURLCredential credentialWithIdentity:identityApp certificates:(__bridge NSArray *)myCerts persistence:NSURLCredentialPersistencePermanent];
        CFRelease(myCerts);

        return credential;

    } else if (result == errSecAuthFailed) {

        _exception = [[NSException alloc] initWithName:@"PKCS12 key store mac invalid" reason:@"PKCS12 key store mac invalid - wrong password or corrupted file." userInfo:NULL];

        @throw _exception;

    } else if (result == errSecDecode) {

        _exception = [[NSException alloc] initWithName:@"PKCS12 key store mac invalid" reason:@"PKCS12 key store mac invalid - unable to decode the provided data." userInfo:NULL];

        @throw _exception;

    }
    
    return 0;

}

#pragma mark NSURLSession Delegate Methods

- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler {
    
    if (challenge.previousFailureCount > 0) {
        
        completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, NULL);
        
    } else if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {

        completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, NULL);

    } else if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodClientCertificate]) {
        
        @try {

            NSURLCredential * credential = [self socketTrustSetup];
            completionHandler(NSURLSessionAuthChallengeUseCredential, credential);

        }
        @catch(NSException *e) {
            
            completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, NULL);
            
        }

    } else {

        completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, NULL);

    }
}

@end
