#import <XCTest/XCTest.h>
#import <OHHTTPStubs/OHHTTPStubs.h>
#import <OHHTTPStubs/OHPathHelpers.h>

#import "HttpRequest.h"

@interface Tests : XCTestCase

@end

@implementation Tests

NSString *fakeBundleWithPass;
NSString *fakeBundleNoPass;

- (void)setUp {

    [super setUp];

    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:@"Info" ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
    fakeBundleWithPass = [dict valueForKey:@"Fake Bundle With Pass"];
    fakeBundleNoPass = [dict valueForKey:@"Fake Bundle No Pass"];

}

- (void)tearDown {
    [super tearDown];
}

- (void)testShouldCreateCredentialWithValidPassword {
    
    @try {

        NSMutableDictionary* opts = [[NSMutableDictionary alloc] init];
        [opts setValue:@"password" forKey:@"password"];
        [opts setValue:@"http://www.foobar.com/" forKey:@"url"];
        [opts setValue:fakeBundleWithPass forKey:@"keystore"];
        
        HttpRequest* request = [[HttpRequest alloc] initWithParams:opts callback:^(NSArray *response) {
            XCTAssert(FALSE);
        }];

        NSURLCredential* credential = [request socketTrustSetup];
        XCTAssert(credential);

    } @catch(NSException *e) {

        XCTAssert(FALSE);

    }

}

- (void)testShouldNotCreateCredentialWithInvalidPassword {

    @try {

        NSMutableDictionary* opts = [[NSMutableDictionary alloc] init];
        [opts setValue:@"password1" forKey:@"password"];
        [opts setValue:@"http://www.foobar.com/" forKey:@"url"];
        [opts setValue:fakeBundleWithPass forKey:@"keystore"];
        
        HttpRequest *request = [[HttpRequest alloc] initWithParams:opts callback:^(NSArray *response) {
            XCTAssert(FALSE);
        }];
        
        NSString *error = @"";

        @try {
            [request socketTrustSetup];
        } @catch(NSException *e) {
            error = [e reason];
        }
        
        XCTAssertEqual(error, @"PKCS12 key store mac invalid - wrong password or corrupted file.");

    } @catch(NSException *e) {

        XCTAssert(FALSE);

    }

}

- (void)testShouldNotRequirePasswordToCreateCredential {
    
    @try {

        NSURLCredential* credential;
        HttpRequest* request;
    
        NSMutableDictionary* opts = [[NSMutableDictionary alloc] init];
        [opts setValue:@"http://www.foobar.com/" forKey:@"url"];
        [opts setValue:fakeBundleNoPass forKey:@"keystore"];
        
        request = [[HttpRequest alloc] initWithParams:opts callback:^(NSArray *response) {
            XCTAssert(FALSE);
        }];
        credential = [request socketTrustSetup];
        XCTAssert(credential);

        [opts setValue:@"" forKey:@"password"];
        
        request = [[HttpRequest alloc] initWithParams:opts callback:^(NSArray *response) {
            XCTAssert(FALSE);
        }];
        credential = [request socketTrustSetup];
        XCTAssert(credential);
        
        [opts setValue:nil forKey:@"password"];

        request = [[HttpRequest alloc] initWithParams:opts callback:^(NSArray *response) {
            XCTAssert(FALSE);
        }];
        credential = [request socketTrustSetup];
        XCTAssert(credential);

    } @catch(NSException *e) {

        XCTAssert(FALSE);

    }

}

- (void)testShouldRespondToRequestsWithSuccessfulMutualAuthentication {
    
    @try {

        XCTestExpectation *expectation = [[XCTestExpectation alloc] initWithDescription:@"Should fulfill request"];
        
        NSMutableDictionary* opts = [[NSMutableDictionary alloc] init];
        [opts setValue:@"password" forKey:@"password"];
        [opts setValue:@"http://www.foobar.com/" forKey:@"url"];
        [opts setValue:fakeBundleWithPass forKey:@"keystore"];
        
        HttpRequest* request = [[HttpRequest alloc] initWithParams:opts callback:^(NSArray *response) {

            XCTAssert([response objectAtIndex:0] == NSNull.null);
            XCTAssert([@"it worked!" isEqualToString:[response objectAtIndex:1]]);
            [expectation fulfill];

        }];
        
        NSURLCredential* credential = [request socketTrustSetup];
        XCTAssert(credential);

        [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
            return [request.URL.host isEqualToString:@"www.foobar.com"];
        } withStubResponse:^OHHTTPStubsResponse*(NSURLRequest *request) {
            return [OHHTTPStubsResponse responseWithData:[@"it worked!" dataUsingEncoding:NSUTF8StringEncoding] statusCode:200 headers:NULL];
        }];
        
        [request get];
        
        [self waitForExpectations:@[expectation] timeout:1];
        
    } @catch(NSException *e) {

        XCTAssert(FALSE);

    }

}

@end
