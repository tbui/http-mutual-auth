#import "HttpMutualAuth.h"
#import "HttpRequest.h"

@implementation HttpMutualAuth

RCT_EXPORT_MODULE();

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

RCT_EXPORT_METHOD(get:(NSDictionary *)opts callback:(RCTResponseSenderBlock)callback)
{
    [[[HttpRequest alloc] initWithParams:opts callback:callback] get];
}

@end
