# http-mutual-auth

## Getting started

```sh
$ yarn add http-mutual-auth
$ # Or, if you are using npm
$ npm install http-mutual-auth --save
```

### Automatic installation

```sh
react-native link http-mutual-auth
```

### Usage

Import the module:

```javascript
import Request from 'http-mutual-auth';
```

The current implementation is very basic - it only supports `get` requests, no headers, and no query string parameters. Depending on your use case, this may not be enough. To perform a `get` request:

```javascript
const p = Request.get(options);
```

Where `p` is a Promise and `options` is an object with the following keys:
* url - url of the request
* keystore - PKCS #12 base 64 encoded keystore
* password - keystore password (optional)

```javascript
Request.get({
  url: 'https://_____.com/',
  keystore: 'a2V5c3RvcmU=',
  password: 'foobar',
});
```

The keystore should contain a private key and a certificate. Various libraries exist for PKCS #12 keystore generation. With node [this project](https://github.com/Dexus/pem) is one option. The binary data has to be base 64 encoded.

Promise should resolve with the response body.

```javascript
Request.get(options).then(body => {
  console.log(body);
});
```

If there's a problem with any of the parameters (for example, invalid keystore password) you should expected to have the promise rejected with an error string beginning with "Parameter error: " as the reason. Otherwise, the error string should begin with "Connection error: ".

```javascript
Request.get({
  ...options,
  password: 'this password is so wrong',
}).catch(e => {
  if(e === 'Parameter error: PKCS12 key store mac invalid - wrong password or corrupted file.') {
    console.error('wrong password or corrupted file');
  }
});
```

Error messages should be the same across platforms but in reality they may not be - i.e., they should be handled in native code and mapped to the same value on both iOS and Android. If you notice a discrepancy, please contribute.

### Contribute

Create a pull request!

Instrumented unit tests are written for both iOS and Android. Sample PKCS #12 assets exist - on Android, they're stored under `src/androidTest/resources` while on iOS they're stored in Tests target's Info.plist file.

For tests to work http-mutual-auth must be part of an existing project. To work from the master branch simply use the git url as this project's version in your package.json file. For example:

```json
{
  "dependencies": {
    "http-mutual-auth": "https://github.com/mateusz-/http-mutual-auth.git",
  }
}
```

Alternatively, create a symlink `node_modules/http-mutual-auth` to this project on your system:

```sh
$ rm -rf node_modules/http-mutual-auth
$ ln -s ../../http-mutual-auth node_modules/http-mutual-auth
```

iOS testing depends on [OKHTTPStubs](https://github.com/AliSoftware/OHHTTPStubs) which needs to be pulled using [Carthage](https://github.com/Carthage/Carthage).

```sh
$ cd ios
$ carthage update
```

Undo your changes after pull request has been accepted and a new version exists.
